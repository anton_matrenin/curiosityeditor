# Curiosity Editor #

![Alt text](https://bitbucket.org/anton_matrenin/curiosityeditor/raw/9d32875aaecadae13e0bb8d63b6c062ee251fbc3/1.jpg)

![Alt text](https://bitbucket.org/anton_matrenin/curiosityeditor/raw/9d32875aaecadae13e0bb8d63b6c062ee251fbc3/2.jpg)


Возможности
---
- Создание и редактирование марсохода
- Перемещение, поворот, масштабирование объектов
- Наличие ортографической и перспективной камеры
- Изменение параметров камеры
- Осуществление функции LookAt
- Сохранение и загрузка сцены
- Ручная система генерации трехмерной графики
