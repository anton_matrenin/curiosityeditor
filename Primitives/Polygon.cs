﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GraphicsEditor
{
    public class Polygon
    {
        public readonly Point3D[] points;

        public Polygon(Point3D[] points)
        {
            this.points = points;
        }

        public void Move(Point3D offset)
        {
            Conversion c = new Conversion();
            for (int i = 0; i < points.Length; i++)
                points[i] = c.Move(points[i], offset);
        }

        public void Scale(Point3D offset, Point3D point)
        {
            Conversion c = new Conversion();
            for (int i = 0; i < points.Length; i++)
                points[i] = c.Scale(points[i], point, offset);
        }

        public void RotateX(Point3D center, float angle)
        {
            Conversion c = new Conversion();
            for (int i = 0; i < points.Length; i++)
                points[i] = c.RotateX(points[i], center, angle);
        }

        public void RotateY(Point3D center, float angle)
        {
            Conversion c = new Conversion();
            for (int i = 0; i < points.Length; i++)
                points[i] = c.RotateY(points[i], center, angle);
        }

        public void RotateZ(Point3D center, float angle)
        {
            Conversion c = new Conversion();
            for (int i = 0; i < points.Length; i++)
                points[i] = c.RotateZ(points[i], center, angle);
        }
    }
}
