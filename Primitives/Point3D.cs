﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GraphicsEditor
{
    public class Point3D
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }
        public float h { get; set; }

        public Point3D()
        {
            x = 0;
            y = 0;
            z = 0;
            h = 1;
        }

        public Point3D(float x, float y, float z, float h)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.h = h;
        }

        public static Point3D operator + (Point3D p1, Point3D p2)
        {
            Point3D cur = new Point3D();
            cur.x = p1.x + p2.x;
            cur.y = p1.y + p2.y;
            cur.z = p1.z + p2.z;
            return cur;
        }

        public static Point3D operator * (Point3D p1, Point3D p2)
        {
            p1.x *= p2.x;
            p1.y *= p2.y;
            p1.z *= p2.z;
            return p1;
        }
    }  
}
