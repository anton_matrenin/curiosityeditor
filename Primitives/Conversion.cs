﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace GraphicsEditor
{

    public class Conversion
    {
        public Point3D Move(Point3D source, Point3D offset)
        {
            float[,] matrix = 
            { 
                              { 1, 0, 0, offset.x }, 
                              { 0, 1, 0, offset.y }, 
                              { 0, 0, 1, offset.z },
                              { 0, 0, 0, 1 }
            };
            return Multiply(source, matrix);
        }

        public Point3D ScaleNormal(Point3D source, Point3D offset)
        {
            float[,] matrix = 
            { 
                              { offset.x, 0, 0, 0 }, 
                              { 0, offset.y, 0, 0 }, 
                              { 0, 0, offset.z, 0 },
                              { 0, 0, 0, 1 }
            };
            var x = Multiply(source, matrix);
            return x;
        }

        public Point3D Scale(Point3D source, Point3D pp, Point3D offset)
        {
            Point3D pp1 = Move(source, new Point3D(-pp.x, -pp.y, -pp.z, -pp.z));
            pp1 = ScaleNormal(pp1, offset);
            pp1 = Move(pp1, new Point3D(pp.x, pp.y, pp.z, pp.z));
            return pp1;
        }

        public Point3D RotateX(Point3D source, Point3D pp, float angle)
        {
            Point3D pp1 = Move(source, new Point3D(-pp.x,-pp.y,-pp.z,-pp.z));
            pp1 = RotateXNormal(pp1, angle);
            pp1 = Move(pp1, new Point3D(pp.x, pp.y, pp.z, pp.z));
            return pp1;
        }

        public Point3D RotateY(Point3D source, Point3D pp, float angle)
        {
            Point3D pp1 = Move(source, new Point3D(-pp.x, -pp.y, -pp.z, -pp.z));
            pp1 = RotateYNormal(pp1, angle);
            pp1 = Move(pp1, new Point3D(pp.x, pp.y, pp.z, pp.z));
            return pp1;
        }

        public Point3D RotateZ(Point3D source, Point3D pp, float angle)
        {
            Point3D pp1 = Move(source, new Point3D(-pp.x, -pp.y, -pp.z, -pp.z));
            pp1 = RotateZNormal(pp1, angle);
            pp1 = Move(pp1, new Point3D(pp.x, pp.y, pp.z, pp.z));
            return pp1;
        }

        private Point3D RotateXNormal(Point3D source, float angle)
        {
            double fi = angle * Math.PI / 180;
            float sin = (float)Math.Sin(fi);
            float cos = (float)Math.Cos(fi);
            float[,] matrix = 
            { 
                              { 1, 0, 0, 0 }, 
                              { 0, cos, sin, 0 }, 
                              { 0, -sin, cos, 0 },
                              { 0, 0, 0, 1 }
            };
            return Multiply(source, matrix);
        }

        private Point3D RotateYNormal(Point3D source, float angle)
        {
            double fi = angle * Math.PI / 180;
            float sin = (float)Math.Sin(fi);
            float cos = (float)Math.Cos(fi);
            float[,] matrix = 
            { 
                              { cos, 0, -sin, 0 }, 
                              { 0, 1, 0, 0 }, 
                              { sin, 0, cos, 0 },
                              { 0, 0, 0, 1 }
            };
            return Multiply(source, matrix);
        }

        private Point3D RotateZNormal(Point3D source, float angle)
        {
            double fi = angle * Math.PI / 180;
            float sin = (float)Math.Sin(fi);
            float cos = (float)Math.Cos(fi);
            float[,] matrix = 
            { 
                              { cos, sin, 0, 0 }, 
                              { -sin, cos, 0, 0 }, 
                              { 0, 0, 1, 0 },
                              { 0, 0, 0, 1 }
            };
            return Multiply(source, matrix);
        }

        private Point3D Multiply(Point3D curPoint, float[,] matrix)
        {
            float[] vector = { curPoint.x, curPoint.y, curPoint.z, curPoint.h };
            float[] res = new float[4];
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                    res[i] += matrix[i,j] * vector[j];
            return new Point3D(res[0], res[1], res[2], res[3]);
        }
    }
}
