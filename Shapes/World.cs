﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GraphicsEditor
{
    
    public class World
    {
        public List<Curiosity> curioList;


        public World()
        {
            curioList = new List<Curiosity>();
            curioList.Add(new Curiosity());
            curioList.Add(new Curiosity());

            curioList[0].Head = new Point3D(200, 100, 100, 1);
            curioList[0].State = new Point3D(300, 10, 500, 1);
            curioList[0].WheelRadius = 50;
            curioList[0].HeadBracketHeight = 180;
            curioList[0].WheelBracketHeight = 100;
            curioList[0].countCamera = 3;
            curioList[0].alpha = 40;
            curioList[0].Update();

            curioList[0].Scale(new Point3D(0.3f, 0.3f, 0.3f, 1));
            curioList[0].Move(new Point3D(300, 0, 0, 1));
            curioList[1].Move(new Point3D(-200, 0, 0, 1));

        }

        public World(List<Curiosity> curio)
        {
            curioList = curio;
            foreach(Curiosity c in curioList)
            {
                c.Update();
            }
        }
    }
}
