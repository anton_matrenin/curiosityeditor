﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicsEditor
{
    public class Shape
    {
        public Polygon[] polygons;
        public Point3D position;
        public Point3D rotation;
        public Point3D scale;

        public void Move(Point3D offset)
        {
            for (int i = 0; i < polygons.Length; i++)
                polygons[i].Move(offset);
            position = position + offset;
        }

        public void Scale(Point3D offset, Point3D point)
        {
            for (int i = 0; i < polygons.Length; i++)
                polygons[i].Scale(offset, point);
            scale = scale * offset;
        }

        public void RotateX(float angle)
        {
            for (int i = 0; i < polygons.Length; i++)
                polygons[i].RotateX(position, angle);
            rotation = rotation + new Point3D(angle,0,0,1);
        }

        public void RotateY(float angle)
        {
            for (int i = 0; i < polygons.Length; i++)
                polygons[i].RotateY(position, angle);
            rotation = rotation + new Point3D(0, angle, 0, 1);
        }

        public void RotateZ(float angle)
        {
            for (int i = 0; i < polygons.Length; i++)
                polygons[i].RotateZ(position, angle);
            rotation = rotation + new Point3D(0, 0, angle, 1);
        }

        public void RotateX(Point3D p, float angle)
        {
            for (int i = 0; i < polygons.Length; i++)
                polygons[i].RotateX(p, angle);
            rotation = rotation + new Point3D(angle, 0, 0, 1);
        }

        public void RotateY(Point3D p, float angle)
        {
            for (int i = 0; i < polygons.Length; i++)
                polygons[i].RotateY(p, angle);
            rotation = rotation + new Point3D(0, angle, 0, 1);
        }

        public void RotateZ(Point3D p, float angle)
        {
            for (int i = 0; i < polygons.Length; i++)
                polygons[i].RotateZ(p, angle);
            rotation = rotation + new Point3D(0, 0, angle, 1);
        }
    }
}
