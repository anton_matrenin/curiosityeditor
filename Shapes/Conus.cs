﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicsEditor
{
    class Conus : Shape
    {
        public const int QUALITY = 8;

        public Conus(Point3D center, float radius, float height)
        {
            Point3D[] p1 = CalculateBasePoint(new Point3D(center.x, center.y, center.z + height / 2, 1), radius);
            Point3D[] p2 = CalculateBaseSecondPoint(p1, height);

            Polygon[] b1 = CreateBase(p1);
            Polygon[] b2 = CreateBase(p2);
            Polygon[] body = CreateBody(p1, p2);

            polygons = new Polygon[QUALITY*4];
            int k = 0;
            for (int i = 0; i < b1.Length; i++)
            {
                polygons[k++] = b1[i];
            }
            for (int i = 0; i < b2.Length; i++)
            {
                polygons[k++] = b2[i];
            }
            for (int i = 0; i < body.Length; i++)
            {
                polygons[k++] = body[i];
            }

            position = center;
            rotation = new Point3D(0, 0, 0, 1);
            scale = new Point3D(1, 1, 1, 1);
        }

        private Point3D[] CalculateBasePoint(Point3D center, float radius)
        {
            Point3D[] res = new Point3D[QUALITY + 1];
            Conversion c = new Conversion();

            float angle = 360 / QUALITY;
            float curAngle = angle;

            res[0] = center;
            res[1] = new Point3D(center.x, center.y + radius, center.z, 1);

            for (int i = 2; i < QUALITY + 1; i++)
            {
                res[i] = c.RotateZ(res[1], center, curAngle);
                curAngle += angle;
            }

            return res;
        }

        private Point3D[] CalculateBaseSecondPoint(Point3D[] fbase, float heigh)
        {
            Point3D[] res = new Point3D[QUALITY + 1];
            for (int i = 0; i < QUALITY + 1; i++)
            {
                res[i] = new Point3D(fbase[i].x, fbase[i].y, fbase[i].z - heigh, 1);
            }
            return res;
        }

        private Polygon[] CreateBase(Point3D[] points)
        {
            Polygon[] res = new Polygon[QUALITY];

            for (int i = 0; i < QUALITY - 1; i++)
            {
                Point3D[] parr = { points[i+1], points[i+2], points[0] };
                res[i] = new Polygon(parr);
            }
            Point3D[] parr2 = { points[1], points[QUALITY], points[0] };
            res[QUALITY - 1] = new Polygon(parr2);

            return res;
        }

        private Polygon[] CreateBody(Point3D[] p1, Point3D[] p2)
        {
            Polygon[] res = new Polygon[QUALITY*2];
            int k = 0;

            for (int i = 1; i < QUALITY; i++)
            {
                Point3D[] parr = { p1[i], p2[i], p2[i+1]};
                Point3D[] parr2 = { p1[i], p1[i + 1], p2[i + 1] };
                res[k++] = new Polygon(parr);
                res[k++] = new Polygon(parr2);
            }

            Point3D[] parr12 = { p1[QUALITY - 1], p2[QUALITY - 1], p2[QUALITY] };
            Point3D[] parr21 = { p1[QUALITY - 1], p1[QUALITY], p2[QUALITY] };
            res[k++] = new Polygon(parr12);
            res[k] = new Polygon(parr21);

            return res;
        }
    }
}
