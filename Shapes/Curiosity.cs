﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GraphicsEditor
{
    public class Curiosity
    {
        [XmlIgnore]
        public Shape[] parts;
        public Point3D positions;
        public Point3D rotation;
        public Point3D scale;
        public float ColissionRadius;

        public Point3D Head; 
        public Point3D State; 
        public float WheelRadius; 
        public float WheelBracketHeight; 
        public float HeadBracketHeight; 
        public int countCamera; 
        public float alpha;

        public Curiosity()
        {
            parts = new Shape[14];
            positions = new Point3D(500, 300, 250, 1);
            rotation = new Point3D(0,0,0,1);
            scale = new Point3D(1,1,1,1);

            State = new Point3D(300, 10, 300, 1);
            Head = new Point3D(130, 60, 70, 1);
            HeadBracketHeight = 140;
            WheelBracketHeight = 200;
            WheelRadius = 30;
            countCamera = 2;
            alpha = 0;

            Update();
        }

        public void Update()
        {
            parts[0] = new Box(positions, State);
            parts[1] = new Box(positions + new Point3D(0, -(HeadBracketHeight + Head.y/2 + State.y/2), 0, 1), Head);

            parts[2] = new Conus(positions + new Point3D(0, -(HeadBracketHeight/2 + State.y/2), 0, 1), 24, HeadBracketHeight);

            parts[3] = new Conus(positions + 
                new Point3D(-(State.x / 2 - 50), State.y / 2 + WheelBracketHeight / 2, State.z / 2 - 50, 1),
                10, WheelBracketHeight);
            parts[4] = new Conus(positions +
                new Point3D(-(State.x / 2 - 50), State.y / 2 + WheelBracketHeight / 2, -(State.z / 2 - 50), 1),
                10, WheelBracketHeight);
            parts[5] = new Conus(positions +
                new Point3D(State.x / 2 - 50, State.y / 2 + WheelBracketHeight / 2, State.z / 2 - 50, 1),
                10, WheelBracketHeight);
            parts[6] = new Conus(positions +
                new Point3D(State.x / 2 - 50, State.y / 2 + WheelBracketHeight / 2, -(State.z / 2 - 50), 1),
                10, WheelBracketHeight);

            parts[7] = new Conus(positions +
                new Point3D(-(State.x / 2 - 50), State.y / 2 + WheelBracketHeight + 15, -(State.z / 2 - 50), 1),
                WheelRadius, 30);
            parts[8] = new Conus(positions +
                new Point3D(-(State.x / 2 - 50), State.y / 2 + WheelBracketHeight + 15, State.z / 2 - 50, 1),
                WheelRadius, 30);
            parts[9] = new Conus(positions +
                new Point3D(State.x / 2 - 50, State.y / 2 + WheelBracketHeight + 15, -(State.z / 2 - 50), 1),
                WheelRadius, 30);
            parts[10] = new Conus(positions +
                new Point3D(State.x / 2 - 50, State.y / 2 + WheelBracketHeight + 15, State.z / 2 - 50, 1),
                WheelRadius, 30);

            float camRadius = 10;
            float camHeight = 10;

            if (countCamera == 1)
            {
                parts[11] = new Conus(parts[1].position + new Point3D(0, 0, Head.z / 2 + camHeight/2, 1), camRadius, camHeight);
                parts[12] = null;
                parts[13] = null;
            }
            if (countCamera == 2)
            {
                parts[11] = new Conus(parts[1].position + new Point3D(Head.x / 4, 0, Head.z / 2 + camHeight / 2, 1), camRadius, camHeight);
                parts[12] = new Conus(parts[1].position + new Point3D(-Head.x / 4, 0, Head.z / 2 + camHeight / 2, 1), camRadius, camHeight);
                parts[13] = null;
            }
            if (countCamera == 3)
            {
                parts[11] = new Conus(parts[1].position + new Point3D(0, 0, Head.z / 2 + camHeight / 2, 1), camRadius, camHeight);
                parts[12] = new Conus(parts[1].position + new Point3D(Head.x/4, 0, Head.z / 2 + camHeight / 2, 1), camRadius, camHeight);
                parts[13] = new Conus(parts[1].position + new Point3D(-Head.x / 4, 0, Head.z / 2 + camHeight / 2, 1), camRadius, camHeight);
            }

            for (int i = 2; i <= 10; i++)
            {
                parts[i].RotateZ(90);
                parts[i].RotateX(90);
            }

            Shape[] HeadShapes = { parts[1], parts[2], parts[11], parts[12], parts[13]};
            foreach(Shape sh in HeadShapes)
            {
                if (sh == null) break;
                sh.RotateY(positions, alpha);
            }

            Point3D newRotate = rotation;
            Point3D newScale = scale;
            rotation = new Point3D();
            scale = new Point3D(1,1,1,1);
            this.Scale(newScale);
            this.RotateX(newRotate.x);
            this.RotateY(newRotate.y);
            this.RotateZ(newRotate.z);

            ColissionRadius = Math.Max(Math.Max(State.x / 2, State.y / 2 + HeadBracketHeight + Head.y), State.y / 2 + WheelBracketHeight + 30) + 30;
        }

        public void Move(Point3D offset)
        {
            foreach (Shape p in parts)
            {
                if (p == null) break;
                p.Move(offset);
            }
            positions = positions + offset;
        }

        public void Scale(Point3D offset)
        {
            foreach (Shape p in parts)
            {
                if (p == null) break;
                p.Scale(offset, positions);
            }
            scale = scale * offset;
        }

        public void RotateX(float angle)
        {
            foreach (Shape p in parts)
            {
                if (p == null) break;
                p.RotateX(positions, angle);
            }
            rotation = rotation + new Point3D(angle,0,0,1);
        }

        public void RotateY(float angle)
        {
            foreach (Shape p in parts)
            {
                if (p == null) break;
                p.RotateY(positions, angle);
            }
            rotation = rotation + new Point3D(0, angle, 0, 1);
        }

        public void RotateZ(float angle)
        {
            foreach (Shape p in parts)
            {
                if (p == null) break;
                p.RotateZ(positions, angle);
            }
            rotation = rotation + new Point3D(0, 0, angle, 1);
        }

        public bool isColission(List<Curiosity> list, int curIndex)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (curIndex == i) 
                    continue;
                if (isColissionTwoCurio(this, list[i]))
                    return true;
            }
            return false;
        }

        public bool isColissionTwoCurio(Curiosity c1, Curiosity c2)
        {
            float d2 = c1.ColissionRadius + c2.ColissionRadius;
            float d1 = (float)Math.Sqrt((double)((c1.positions.x - c2.positions.x) * (c1.positions.x - c2.positions.x) +
                (c1.positions.y - c2.positions.y) * (c1.positions.y - c2.positions.y) +
                (c1.positions.z - c2.positions.z) * (c1.positions.z - c2.positions.z)
                ));
            if (d1 < d2)
                return true;
            else
                return false;
        }
    }
}
