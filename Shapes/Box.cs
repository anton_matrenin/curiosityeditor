﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicsEditor
{
    class Box : Shape
    {
        public Box(Point3D center, Point3D size)
        {
            polygons = new Polygon[12];

            Point3D p1 = new Point3D(center.x - size.x / 2, center.y - size.y / 2, center.z - size.z / 2, 1);
            Point3D p2 = new Point3D(center.x - size.x / 2, center.y + size.y / 2, center.z - size.z / 2, 1);
            Point3D p3 = new Point3D(center.x + size.x / 2, center.y + size.y / 2, center.z - size.z / 2, 1);
            Point3D p4 = new Point3D(center.x + size.x / 2, center.y - size.y / 2, center.z - size.z / 2, 1);

            Point3D p5 = new Point3D(center.x - size.x / 2, center.y - size.y / 2, center.z + size.z / 2, 1);
            Point3D p6 = new Point3D(center.x - size.x / 2, center.y + size.y / 2, center.z + size.z / 2, 1);
            Point3D p7 = new Point3D(center.x + size.x / 2, center.y + size.y / 2, center.z + size.z / 2, 1);
            Point3D p8 = new Point3D(center.x + size.x / 2, center.y - size.y / 2, center.z + size.z / 2, 1);

            Point3D[] polygon1 = { p1, p2, p3};
            Point3D[] polygon2 = { p1, p3, p4 };

            Point3D[] polygon3 = { p5, p6, p7 };
            Point3D[] polygon4 = { p5, p7, p8 };

            Point3D[] polygon5 = { p1, p2, p6 };
            Point3D[] polygon6 = { p1, p6, p5 };

            Point3D[] polygon7 = { p1, p4, p8 };
            Point3D[] polygon8 = { p1, p8, p5 };

            Point3D[] polygon9 = { p4, p8, p3 };
            Point3D[] polygon10 = { p8, p3, p7 };

            Point3D[] polygon11 = { p2, p3, p7 };
            Point3D[] polygon12 = { p2, p7, p6 };

            polygons[0] = new Polygon(polygon1);
            polygons[1] = new Polygon(polygon2);
            polygons[2] = new Polygon(polygon3);
            polygons[3] = new Polygon(polygon4);
            polygons[4] = new Polygon(polygon5);
            polygons[5] = new Polygon(polygon6);
            polygons[6] = new Polygon(polygon7);
            polygons[7] = new Polygon(polygon8);
            polygons[8] = new Polygon(polygon9);
            polygons[9] = new Polygon(polygon10);
            polygons[10] = new Polygon(polygon11);
            polygons[11] = new Polygon(polygon12);

            position = center;
            rotation = new Point3D(0, 0, 0, 1);
            scale = new Point3D(1, 1, 1, 1);
        }
    }
}
