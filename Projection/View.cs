﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GraphicsEditor
{
    public class View
    {
        public const int PROJECTION_XYZ = 0;
        public const int PROJECTION_XY = 1;
        public const int PROJECTION_XZ = 2;
        public const int PROJECTION_YZ = 3;

        public int currentView;

        public View()
        {
            currentView = PROJECTION_XY;
        }

        public PointF[] Create2DPolygon(Polygon poly)
        {
            PointF[] res = new PointF[poly.points.Length];
            for (int i = 0; i < poly.points.Length; i++)
            {
                switch (currentView)
                {
                    case PROJECTION_XY: 
                        res[i] = new PointF(poly.points[i].x, poly.points[i].y);
                        break;
                    case PROJECTION_XZ:
                        res[i] = new PointF(poly.points[i].x, poly.points[i].z);
                        break;
                    case PROJECTION_YZ:
                        res[i] = new PointF(poly.points[i].y, poly.points[i].z);
                        break;
                }                
            }
            return res;
        }
    }
}
