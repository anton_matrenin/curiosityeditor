﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace GraphicsEditor
{
    class Serializer
    {
        public void Serialize(List<Curiosity> list, string fpath)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Curiosity));
            FileStream fs = new FileStream(fpath, FileMode.Create);
            foreach(Curiosity c in list)
            {
                serializer.Serialize(fs, c);
            }
            fs.Close(); 
        }

        public Curiosity Deserialize()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Curiosity));
            FileStream fs = new FileStream(@"e:\file.xml", FileMode.Open);
            Curiosity c = (Curiosity)serializer.Deserialize(fs);
            fs.Close();
            return c;
        }
    }
}
